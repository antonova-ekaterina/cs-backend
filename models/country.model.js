const Sequelize = require('sequelize');
const sequelize = require('../configs/sequelize.config').sequelize;

const User = sequelize.define('country', {
  name: Sequelize.STRING,
  year: Sequelize.INTEGER,
  x1: Sequelize.REAL,
  x2: Sequelize.REAL,
  x3: Sequelize.REAL,
  x4: Sequelize.REAL,
  x5: Sequelize.REAL,
  x6: Sequelize.REAL,
  officialRating: Sequelize.STRING,
  discriminantRating: Sequelize.STRING,
  neuralNetworkSNNRating: Sequelize.STRING,
  neuralNetworkMatlabRating: Sequelize.STRING,
  fuzzyRating: Sequelize.STRING
});

module.exports = User;