let devConfig={
  server:{
    port: 3000,
    host: '127.0.0.1'
  },
  db: {
    port: 5431,
    host: '127.0.0.1',
    name: 'cs',
    userName: 'postgres',
    password: '1'
  },
  morgan: {
    format: 'dev'
  },
  winston: {
    loggingLevel: 'debug'
  }
}

module.exports = devConfig;