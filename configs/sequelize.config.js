const Sequelize = require('sequelize');
const winston = require('./winston.config');
const config = require('./');

const sequelize = new Sequelize(config.db.name, config.db.userName, config.db.password, {
  host: config.db.host,
  port: config.db.port,
  dialect: 'postgres',
  operatorsAliases: false,

});

exports.connect = () =>
  sequelize
    .authenticate()
    .then(() => {
      winston.info('Connection has been established successfully.');
    })
    .catch(err => {
      winston.error('Unable to connect to the database:', err);
    });

module.exports.sequelize = sequelize;
