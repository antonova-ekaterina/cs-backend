const services = require('../services');
const winston = require('../configs/winston.config');

countryController = {

  getAll: (req, res, next) => {
    return services.country.getAll()
      .then((rows) => {
        req.dataOut = rows;
        next();
      })
      .catch((err) => {
        winston.error(`error in getAll in countryController = ${err}`);
        next(err);
      })
  },

  getCountriesList: (req, res, next) => {
    return services.country.getCountriesList()
      .then((countries) => {
        req.dataOut = countries;
        next();
      })
      .catch((err) => {
        winston.error(`error in getCountriesList in countryController = ${err}`);
        next(err);
      })
  },

  getDataForYear: (req, res, next) => {
    return services.country.getDataForYear(req.query.year)
      .then((countries) => {
        req.dataOut = countries;
        next();
      })
      .catch((err) => {
        winston.error(`error in getDataForYear in countryController = ${err}`);
        next(err);
      })
  },
};

module.exports = countryController;
