const sequelize = require('../configs/sequelize.config');
const services = require('../services');
const winston = require('../configs/winston.config');
const xlsx = require('node-xlsx');

sequelize.connect()
  .then(() => {
    return services.country.createTable();
  })
  .then(() => {

    // Parse a file
    const workSheetsFromFile = xlsx.parse('data.xlsx');
    const ratings = workSheetsFromFile[1].data;
    ratings.splice(0, 2);

    ratings.map((rating) => {
      console.log('------------------------');
      let country = {};
      country.name = rating[1].slice(0, rating[1].indexOf('_'));
      country.year = '20'+ rating[1].slice(rating[1].indexOf('_') + 1, 100);
      country.x1 = rating[2];
      country.x2 = rating[3];
      country.x3 = rating[4];
      country.x4 = rating[5];
      country.x5 = rating[6];
      country.x6 = rating[7];
      country.officialRating = 'A';
      country.discriminantRating = rating[9];
      country.neuralNetworkSNNRating = rating[11];
      country.neuralNetworkMatlabRating = rating[12];
      country.fuzzyRating = rating[14];
      console.log(country);

      return services.country.addCountry(country);
    });

  })
  .then(() => {
    console.log('done!');
  })
  .catch((err) => {
    winston.error(`error in modelCreation = ${err}`);
  });
