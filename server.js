const config = require('./configs');
const routes = require('./routes');
const sequelize = require('./configs/sequelize.config');
const winston = require('./configs/winston.config');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

const express = require('express');
const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  next();
});
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(routes);

app.use((req, res, next) => {
  if (req.dataOut) {
    res.send({
      success: 'true',
      data: req.dataOut
    })
  }else{
    next('api not found');
  }
});

app.use((err, req, res, next) => {
  console.log('here')
  winston.info(`Error in req: ${JSON.stringify(err)}`);
  res.send({
    success: false,
    error: err
  })
})

sequelize.connect()
  .then(() => {
    app.listen(config.server.port, config.server.host, (err) => {
      if (err) {
        return winston.error(`error in starting server ${err}`);
      }
      winston.info(`server was started at port ${config.server.port}`);
    });
  })

