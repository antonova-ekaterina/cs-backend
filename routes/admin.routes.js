const router = require('express').Router();
const adminController = require('../controllers/admin.controller');
const { body } = require('express-validator/check');

router.post('/login', [
    body('email').not().isEmpty().isLength({ max: 20 }),
    body('password').not().isEmpty().isLength({ max: 20 })
  ],
  adminController.login
);

module.exports = router;