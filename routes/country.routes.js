const router = require('express').Router();
const countryController = require('../controllers/country.controller');

router.get('/',
  countryController.getAll
);

router.get('/all',
  countryController.getCountriesList
);

router.get('/year',
  countryController.getDataForYear
);

module.exports = router;
