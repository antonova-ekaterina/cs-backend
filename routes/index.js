const morgan = require('./morgan.routes');
const statisticsRoutes = require('./statistics.routes');
const countryRoutes = require('./country.routes');
const adminRoutes = require('./admin.routes');
const router = require('express').Router();

router.use(morgan);
router.use('/stat', statisticsRoutes);
router.use('/country', countryRoutes);
router.use('/admin', adminRoutes);

module.exports = router;
