const db = require('../models');
const winston = require('../configs/winston.config');

const countryService = {

  createTable() {
    return db.country.sync({force: true})
      .then(() => {
        winston.info('table Country created');
      });
  },

  addCountry(country) {
    return db.country.create({
      name: country.name,
      year: country.year,
      x1: country.x1,
      x2: country.x2,
      x3: country.x3,
      x4: country.x4,
      x5: country.x5,
      x6: country.x6,
      officialRating: country.officialRating,
      discriminantRating: country.discriminantRating,
      neuralNetworkSNNRating: country.neuralNetworkSNNRating,
      neuralNetworkMatlabRating: country.neuralNetworkMatlabRating,
      fuzzyRating: country.fuzzyRating
    })
  },

  getAll() {
    return db.country.findAll()
      .then((rows) => {
        return rows;
      })
      .catch((err) => {
        winston.error(`error in getAll in country service = ${err}`);
        throw {
          code: 1111,
          description: 'database'
        }
      })
  },

  getCountriesList() {
    return db.country.findAll({
      attributes: ['name']
    })
      .then((countries) => {
        return countries;
      })
      .catch((err) => {
        winston.error(`error in getCountriesList in country service = ${err}`);
        throw {
          code: 1111,
          description: 'database'
        }
      })
  },

  getDataForYear(year){
    return db.country.findAll({
      where: {
        year: year
      }
    })
      .then((countries) => {
        return countries;
      })
      .catch((err) => {
        winston.error(`error in getDataForYear in country service = ${err}`);
        throw {
          code: 1111,
          description: 'database'
        }
      })
  }

}

module.exports = countryService;