const countryService = require('./country.service');

const services = {
  country: countryService
};

module.exports = services;